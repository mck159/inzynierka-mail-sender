package pl.edu.agh.msroka.pracaInzynierska.mailingServer.daos;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.daos.utils.GenericCRUDDao;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities.Mail;

import java.util.List;

@Repository
public class MailDAO extends GenericCRUDDao<Mail, Integer> {
    private List<Mail> allUnsendedEmails;

    public MailDAO() {
        super(Mail.class);
    }

    public List<Mail> getAllUnsendedEmails() {
        Session currentSession = getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Mail.class);
        criteria.add(Restrictions.eq("wasSended", false));
        return criteria.list();
    }

    public void setAllUnsendedEmails(List<Mail> allUnsendedEmails) {
        this.allUnsendedEmails = allUnsendedEmails;
    }
//
//    public List<Client> getNewestClientsWithDistinctNIPsByCompany(Company company) {
//        Session session = getCurrentSession();
//        int company_id = company.getId();
//        String queryString = String.format("SELECT c.* FROM clients c INNER JOIN ( SELECT MAX(updated_at) updated_at, nip FROM clients WHERE company_id='%d' GROUP BY nip) s ON s.nip = c.nip AND s.updated_at = c.updated_at", company_id);
//        SQLQuery query = session.createSQLQuery(queryString).addEntity(Client.class);
//        return query.list();
//    }
}
