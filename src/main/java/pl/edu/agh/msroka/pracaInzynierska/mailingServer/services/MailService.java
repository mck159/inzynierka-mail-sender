package pl.edu.agh.msroka.pracaInzynierska.mailingServer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.daos.MailDAO;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.dto.DTOConverter;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.dto.MailDTO;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities.Mail;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.services.mailing.MailerService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class MailService {
    @Autowired
    MailDAO mailDAO;

    @Autowired
    MailerService mailerService;

    @PersistenceContext
    EntityManager em;

    @Autowired
    DTOConverter dtoConverter;

    private Object allEmails;


    public void handleMail(MailDTO mailDTO) {
        if(mailDTO.getImmediate() == true) {
            Mail mail = dtoConverter.fromMailDTO(mailDTO);
            if(mailerService.sendEmail(mail)) {
                mail.setWasSended(true);
                this.save(mail);
            } else {
                //TODO retry logic
            }
        } else {
            Mail mail = new Mail();
            mail.setBody(mailDTO.getBody());
            mail.setSubject(mailDTO.getSubject());
            mail.setWasSended(false);
            mail.setRecipients(mailDTO.getRecipients());
            em.persist(mail);
        }
    }

    public List<Mail> getAllUnsendedEmails() {
        return mailDAO.getAllUnsendedEmails();
    }

    public void setAllEmails(Object allEmails) {
        this.allEmails = allEmails;
    }

    public void save(Mail mail) {
        em.merge(mail);
    }
}
