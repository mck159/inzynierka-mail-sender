package pl.edu.agh.msroka.pracaInzynierska.mailingServer.dto;

import org.springframework.stereotype.Component;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities.Mail;

@Component
public class DTOConverter {
    public MailDTO toMailDTO(Mail mail) {
        MailDTO mailDTO = new MailDTO();
        mailDTO.setRecipients(mail.getRecipients());
        mailDTO.setBody(mail.getBody());
        mailDTO.setSubject(mail.getSubject());
        return mailDTO;
    }

    public Mail fromMailDTO(MailDTO mailDTO) {
        Mail mail = new Mail();
        mail.setRecipients(mailDTO.getRecipients());
        mail.setBody(mailDTO.getBody());
        mail.setSubject(mailDTO.getSubject());
        return mail;
    }
}
