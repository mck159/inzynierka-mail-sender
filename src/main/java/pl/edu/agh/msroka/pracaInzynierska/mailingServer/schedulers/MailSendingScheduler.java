package pl.edu.agh.msroka.pracaInzynierska.mailingServer.schedulers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities.Mail;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.services.MailService;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.services.mailing.MailerService;

import java.util.List;

@Component
public class MailSendingScheduler {
    @Autowired
    MailService mailService;
    @Autowired
    MailerService mailerService;

    @Scheduled(cron = "*/10 * * * * *") // every minute
    public void sendEmailsEveryHour() {
        List<Mail> mails = mailService.getAllUnsendedEmails();
        for(Mail mail : mails) {
            if(mailerService.sendEmail(mail) == true) {
                mail.setWasSended(true);
                mailService.save(mail);
            }
        }
    }
}
