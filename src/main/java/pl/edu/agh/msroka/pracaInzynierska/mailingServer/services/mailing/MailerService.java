package pl.edu.agh.msroka.pracaInzynierska.mailingServer.services.mailing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.daos.MailDAO;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.dto.MailDTO;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities.Mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
@Transactional
public class MailerService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    JavaMailSender mailSender;

    public boolean sendEmail(Mail mail) {
        System.out.println("SENDING EMAIL");

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            for(String recipient : mail.getRecipients()) {
                helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
                mimeMessage.setContent(mail.getBody(), "text/html");
                helper.setTo(recipient);
                helper.setSubject(mail.getSubject());
                helper.setFrom("test");
                mailSender.send(mimeMessage);
            }

        } catch (Exception e) {
            logger.warn("Mail", "COULD NOT SEND EMAIL " + mail.getId());
            return false;
        }

        return true;
    }
}
