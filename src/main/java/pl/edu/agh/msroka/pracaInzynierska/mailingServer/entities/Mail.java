package pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities;

import pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities.utils.TimestampEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="mails")
public class Mail extends TimestampEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    int id;
    String subject;
    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="mails_recipients",joinColumns=@JoinColumn(name="mail_id"))
    @Column(name="mail")
    List<String> recipients;
    String body;
    Boolean wasSended;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getWasSended() {
        return wasSended;
    }

    public void setWasSended(Boolean wasSended) {
        this.wasSended = wasSended;
    }
}