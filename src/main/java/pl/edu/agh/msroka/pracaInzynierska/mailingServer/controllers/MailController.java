package pl.edu.agh.msroka.pracaInzynierska.mailingServer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.dto.MailDTO;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.entities.Mail;
import pl.edu.agh.msroka.pracaInzynierska.mailingServer.services.MailService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/mail")
public class MailController {
    @Autowired
    MailService mailService;

    @RequestMapping(value="", method = RequestMethod.POST)
    @ResponseBody
    void getNewestClientsWithDistinctNIPs(@RequestBody MailDTO mailDTO) {
        mailService.handleMail(mailDTO);
        return;
    }
}
